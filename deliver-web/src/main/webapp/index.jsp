<html>
<head>
<title></title>
</head>
<body>
	<div style="margin-left: 40px;">
		<span style="font-size: 16px;"><em><strong>Deliver</strong></em></span>
	</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 80px;">
		<span style="font-size: 14px;"><em>O Projeto &eacute; um
				novo sistema de entrega de mercadorias que possui um diferencial
				importante, prover o menor caminho de entrega poss&iacute;vel diminuindo assim
				consideravelmente os custos. Vale salienta que todos os mapas devem
				ser persistidos para composi&ccedil;&atilde;o de uma base hist&oacute;rica para
				futura minera&ccedil;&atilde;o de dados, visando prover mais
				informa&ccedil;&otilde;es para prospec&ccedil;&atilde;o de novos
				neg&oacute;cios.&nbsp;</em></span>
	</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">
		<em><strong>Arquitetura&nbsp;</strong></em>
	</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 80px;">
		<span style="font-size: 14px;">Para implementa&ccedil;&atilde;o do
			diferencial do sistema de entregas que visa obter o menor custo, foi
			estudado a implementa&ccedil;&atilde;o do problema cl&aacute;ssico do
			Cacheiro Viajante, cujo objetivo de determinar o menor circuito dado um
			determinado mapa, vale salientar que os mapas devem ser persistidos na
			base de dados. Visando atender esta necessidade foi definido a
			utiliza&ccedil;&atilde;o de um banco de grafos Neo4j.</span>
	</div>
	<div style="margin-left: 80px;">&nbsp;</div>
	<div style="margin-left: 80px;">
		<span style="font-size: 14px;">Ap&oacute;s a
			defini&ccedil;&atilde;o da base dados foi iniciado a
			cria&ccedil;&atilde;o da estrutura da aplica&ccedil;&atilde;o. A
			aplica&ccedil;&atilde;o foi divida em quatro m&oacute;dulos (beans,
			repository, services e web) objetivando aumenta a coes&atilde;o e
			diminuir o acoplamento entre as camadas (m&oacute;dulos) aumentando
			consideravelmente a manutenibilidade que &eacute; apoiada com a
			utiliza&ccedil;&atilde;o do FrameWork Spring Data.&nbsp;</span>
	</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 80px;">
		<span style="font-size: 14px;">As funcionalidades do sistema
			foram expostas como APIs REST atrav&eacute;s do framework</span>
	</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">
		<em><strong>AVISO</strong></em>
	</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 80px;">
		<span style="font-size: 14px;"><em>Por padr&atilde;o, o
				aplicativo tentar&aacute; usar uma inst&acirc;ncia Neo4j rodando na
				mesma m&aacute;quina no endere&ccedil;o http://localhost:7474, por
				padr&atilde;o durante a execu&ccedil;&atilde;o do teste &nbsp;e
				feito um expurgo total da base &nbsp;o que vai destruir todos os
				dados do banco. Ent&atilde;o, se voc&ecirc; n&atilde;o quer
				que isso aconte&ccedil;a, favor fazer backup do banco
				de dados existente em primeiro lugar.</em></span>
	</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">
		<em><strong>Pr&eacute; -Requisitos</strong></em>
	</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<ul>
		<li style="margin-left: 80px;"><span style="font-size: 14px;">Java
				7 ou superior</span></li>
		<li style="margin-left: 80px;"><span style="font-size: 14px;">Neo4J</span></li>
	</ul>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">
		<strong>Configura&ccedil;&atilde;o</strong>
	</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 80px;">
		<span style="font-size: 14px;">Antes de iniciarmos a
			aplica&ccedil;&atilde;o &eacute; importante realizar a
			configura&ccedil;&atilde;o de acesso a base de dados no nosso caso Neo4j. Para
			isso devemos altera as configura&ccedil;&otilde;es do arquivo
			ogm.properties que esta localizado no m&oacute;dulo
			deliver-repository/src/main/resources.</span>
	</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">
		<strong>Instala&ccedil;&atilde;o</strong>
	</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 80px;">
		<span style="font-size: 14px;">Para instala&ccedil;&atilde;o e
			necess&aacute;rio realizar o download do c&oacute;digo-fonte git
			clone https://&lt; Usuario &gt;@bitbucket.org/rodrigorcarv/deliver.git .</span>
	</div>
	<div style="margin-left: 80px;">&nbsp;</div>
	<div style="margin-left: 80px;">
		<span style="font-size: 14px;">Ap&oacute;s realizar o download
			do c&oacute;digo-fonte deve ser executados os comandos abaixo:</span>
	</div>
	<ul>
		<li style="margin-left: 120px;"><span style="font-size: 14px;">mvn
				eclipse:eclipse &nbsp;</span></li>
		<li style="margin-left: 120px;"><span style="font-size: 14px;">mvn
				clean install</span></li>
	</ul>
	<div style="margin-left: 80px;">
		<span style="font-size: 14px;">A seguir basta copiar o WAR
			dispon&iacute;vel no m&oacute;dulo .../deliver-web/target/deliver-web.war e
			copi&aacute;-lo para o diret&oacute;rio TOMCAT_HOME/webapps.</span>
	</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">
		<strong>Explorando API REST</strong>
	</div>
	<div style="margin-left: 80px;">&nbsp;</div>
	<ul>
		<li style="margin-left: 80px;"><em><strong>SaveRoutes</strong></em></li>
	</ul>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">PUT
			/deliver-web/mapa/saveRoutes/{nome do mapa} HTTP/1.1</span>
	</div>
	<div style="margin-left: 120px;">&nbsp;</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">Host: localhost:9080</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">Accept: text/plain</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">Content-Type: text/plain</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">Cache-Control: no-cache</span>
	</div>
	<div style="margin-left: 120px;">&nbsp;</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">A B 10</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">B D 15&nbsp;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">A C 20&nbsp;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">C D 30&nbsp;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">B E 50&nbsp;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">D E 30</span>
	</div>
	<div style="margin-left: 120px;">&nbsp;</div>
	<ul>
		<li style="margin-left: 80px;"><em><strong>Save</strong></em></li>
	</ul>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">PUT
			/deliver-web/mapa/salvar/{nome do mapa} HTTP/1.1</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">Host: localhost:9080</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">Accept: application/json</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">Content-Type: application/json</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">Cache-Control: no-cache</span>
	</div>
	<div style="margin-left: 120px;">&nbsp;</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">[</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;cidadeOrigem&quot;: {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; &nbsp;
			&quot;nome&quot;: &quot;A&quot;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;cidadeDestino&quot;: {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; &nbsp;
			&quot;nome&quot;: &quot;C&quot;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;distancia&quot;: 20</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;cidadeOrigem&quot;: {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; &nbsp;
			&quot;nome&quot;: &quot;B&quot;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;cidadeDestino&quot;: {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; &nbsp;
			&quot;nome&quot;: &quot;E&quot;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;distancia&quot;: 50</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;cidadeOrigem&quot;: {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; &nbsp;
			&quot;nome&quot;: &quot;B&quot;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;cidadeDestino&quot;: {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; &nbsp;
			&quot;nome&quot;: &quot;D&quot;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;distancia&quot;: 15</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;cidadeOrigem&quot;: {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; &nbsp;
			&quot;nome&quot;: &quot;D&quot;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;cidadeDestino&quot;: {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; &nbsp;
			&quot;nome&quot;: &quot;E&quot;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;distancia&quot;: 30</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;cidadeOrigem&quot;: {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; &nbsp;
			&quot;nome&quot;: &quot;A&quot;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;cidadeDestino&quot;: {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; &nbsp;
			&quot;nome&quot;: &quot;B&quot;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;distancia&quot;: 10</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;cidadeOrigem&quot;: {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; &nbsp;
			&quot;nome&quot;: &quot;C&quot;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;cidadeDestino&quot;: {</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; &nbsp;
			&quot;nome&quot;: &quot;D&quot;</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp; },</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; &nbsp;
			&quot;distancia&quot;: 30</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">&nbsp; }</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">]</span>
	</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<ul>
		<li style="margin-left: 80px;"><em><strong>shortestRouteDeliver</strong></em></li>
	</ul>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">GET
			/deliver-web/mapa/shortestRouteDeliver?mapaNome={nome do
			mapa}&amp;origem={cidade de origem}&amp;destino={cidade de
			destino}&amp;autonomia={autonomia}&amp;valorCombustivel={valor do
			combustivel} HTTP/1.1</span>
	</div>
	<div style="margin-left: 120px;">&nbsp;</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">Host: localhost:9080</span>
	</div>
	<div style="margin-left: 120px;">
		<span style="font-size: 14px;">Cache-Control: no-cache</span>
	</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div style="margin-left: 40px;">&nbsp;</div>
	<div>&nbsp;</div>
</body>
</html>
