package br.com.rrc.deliver.web.controller;

import java.util.Set;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.rrc.deliver.beans.Cidade;
import br.com.rrc.deliver.beans.Rota;
import br.com.rrc.deliver.dto.DadosEntrega;
import br.com.rrc.deliver.dto.DadosEnvio;
import br.com.rrc.deliver.services.RotaService;
import br.com.rrc.deliver.web.convert.RotaConvert;
import br.com.rrc.deliver.wrapper.RotaWrapper;


@RestController
@RequestMapping(value="/mapa")
public class MapaController {
	
	@Autowired 
	private RotaService rotaService;
	
	@Autowired
	private RotaConvert rotaConvert;
	
	/**@PathParam
	 * Salva mapa recebendo o nome vai Path param e as {@link Rotas} (s) via plain 
	 *
	 * @param mapa nome do mapa
	 * @param coordenadas {@link Set} de {@link Rota}
	 */
	@RequestMapping(value="/saveRoutes/{nomeMapa}", method= RequestMethod.PUT, consumes = "text/plain")
	public void saveCoordinates (@PathVariable(value = "nomeMapa") String nomeMapa,  @RequestBody String coordenadas){
		
		Set<Rota> rotas = rotaConvert.stringToRota(coordenadas, nomeMapa);

		validaRotas(rotas, nomeMapa);
		rotaService.save(rotas, nomeMapa);
	}
	
	/** 
	 * Salva mapa recebendo o nome vai Path param e as {@link Rotas} (s) via JSON
	 *
	 * @param mapa nome do mapa
	 * @param {@link Set} de {@link Rota}
	 */
	@RequestMapping(value="/save/{nomeMapa}", method= RequestMethod.PUT, consumes = "application/json")
	public void save (@PathVariable(value = "nomeMapa") String nomeMapa, @RequestBody RotaWrapper rotas){
		
		validaRotas(rotas, nomeMapa);
		rotaService.save(rotas , nomeMapa);
	}
	
	/**
	 * Busca melhor rota de possivel de acordo com os parametros informados calculado o custo da entrega.
	 *
	 * @param mapaNome nome do mapa
	 * @param origem nome ponto (cidade) origem
	 * @param destino nome ponto (cidade) destino
	 * @param autonomia autonomia do veiculo
	 * @param valorCombustivel valor do combustivel
	 * 
	 * @return Json com {@link DadosEntrega}
	 */
	@RequestMapping(value="/shortestRouteDeliver",  method= RequestMethod.GET, produces = "application/json")
	public @ResponseBody DadosEntrega shortestRouteDeliver (	
			@RequestParam ("mapaNome") String mapaNome,
			@RequestParam ("origem") String origem,
			@RequestParam ("destino") String destino,
			@RequestParam ("autonomia") Long autonomia,
			@RequestParam ("valorCombustivel") String valorCombustivel){
		
		DadosEnvio dadosEnvio = new DadosEnvio();
		dadosEnvio.setNomeMapa(mapaNome);
		dadosEnvio.setOrigem(origem);
		dadosEnvio.setDestino(destino);
		dadosEnvio.setAutonomia(autonomia);
		dadosEnvio.setValorCombustivel(valorCombustivel);
		
		validaDadosEnvio(dadosEnvio);
		DadosEntrega dadosEntrega = rotaService.shortestRouteDeliver(dadosEnvio);
		
		return dadosEntrega;
	}
	
	/**
	 * Valida se as {@link Rota} informadas estao corretas.
	 * 
	 *  Tem que ter no minimo 1 rota
	 *  
	 *  O objeto rota obrigatorimente deve ter cidade origem, cidade destino e distancia 
	 *  preenchidas.
	 *  
	 *  A propriedade distancia deve ser maior que zero
	 *  A propriedade Cidade de origem e destino nao podem ser nulas ou vazias.
	 *
	 * @param rotas {@link Rotas}
	 * @param mapa nome do mapa 
	 */
	private void validaRotas(Set<Rota> rotas, String mapa) {
		
		if (rotas == null || rotas.size() <=0 ) {
			throw new IllegalArgumentException("Favor informar no minimo uma rota");
		}
		
		if (StringUtils.isBlank(mapa)) {
			throw new IllegalArgumentException("Favor informar o nome do mapa");
		}
		
		for (Rota rota : rotas) {
			
			validaCidade(rota.getCidadeOrigem());
			validaCidade(rota.getCidadeDestino());
			validaDistancia(rota.getDistancia());
		}
	}
	
	
	/**
	 * Valida se a {@link Cidade} esta preenchida corretante
	 *
	 * O objeto cidade nao pode ser null
	 * 
	 * A propriedade nome nao pode esta nula ou vazia
	 *
	 * @param cidade {@link Cidade}
	 */
	private void validaCidade(Cidade cidade) {
		
		if (cidade == null ) {
			throw new IllegalArgumentException("Favor informar a cidade corretamente");
		}
		
		if (StringUtils.isBlank(cidade.getNome())) {
			throw new IllegalArgumentException("O nome da cidade nao foi informado corretamente");
		}
	}
	
	/**
	 * Valida distancia.
	 *	
	 * A distancia nao pode ser nula ou menor ou igual a zero.
	 *
	 * @param distancia 
	 */
	private void validaDistancia(Long distancia) {
		
		if (distancia == null || distancia <= 0) {
			throw new IllegalArgumentException("Favor informar a distancia corretamente");
		}
	}
	
	/**
	 * Valida {@link DadosEnvio}
	 * 
	 * 	campo e validacoes
	 * 
	 * 		origem nao pode ser null ou vazio
	 * 		destino nao pode ser null ou vazio
	 * 		nomeMapa nao pode ser null ou vazio
	 * 		valorCombustivel nao pode ser null
	 * 		autonomia nao pode ser null ou menor que zero.
	 *
	 * @param dadosEnvio {@link DadosEnvio}
	 */
	private void validaDadosEnvio(DadosEnvio dadosEnvio) {

		if (dadosEnvio == null) {
			throw new IllegalArgumentException("Favor informar corretamente os dados de envio");
		}
		
		if (StringUtils.isBlank(dadosEnvio.getOrigem())) {
			throw new IllegalArgumentException("Favor informar corretamente o ponto (Cidade) de origim");
		}
		
		if (StringUtils.isBlank(dadosEnvio.getDestino())) {
			throw new IllegalArgumentException("Favor informar corretamente o ponto (Cidade) de destino");
		}
		
		if (StringUtils.isBlank(dadosEnvio.getNomeMapa())) {
			throw new IllegalArgumentException("Favor informar corretamente o nome do mapa");
		}
		
		if (StringUtils.isBlank(dadosEnvio.getValorCombustivel())) {
			throw new IllegalArgumentException("Favor informar corretamente o valor do combustivel");
		}

		if (dadosEnvio.getAutonomia() == null || dadosEnvio.getAutonomia() < 0) {
			throw new IllegalArgumentException("Favor informar corretamente a autonomia");
		}
	}
	
}
