package br.com.rrc.deliver.web.convert;

import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;

import br.com.rrc.deliver.beans.Cidade;
import br.com.rrc.deliver.beans.Rota;

@Component
public class RotaConvert {

	//Regex para identifica quebra linhas
	private static final String LINE = "\r\n";
	
	//Regex para identifica origem, destino e distancia
	private static final String COORDENADAS = "\\s*(\\w+)\\s*(\\w+)\\s*(\\d+)\\s*";
	
	/**
	 * Conter de String(text/plain) para um {@link Set} de {@link Rota} 
	 * onde cada linha do plain vai se torna uma rota
	 * 
	 * @param caminho linha contendo cidade (ponto) origim e destino
	 * @param mapa nome do mapa
	 * @return {@link Set} de {@link Rota}
	 */
	public Set<Rota> stringToRota(String caminho, String mapa) {

		Pattern patternLine = Pattern.compile(COORDENADAS);

		StringTokenizer linesTokenizer = new StringTokenizer(caminho, LINE);

		Set<Rota> rotas = new HashSet<Rota>();

		while (linesTokenizer.hasMoreTokens()) {

			final String line = linesTokenizer.nextToken();

			final Matcher matcher = patternLine.matcher(line);

			if (!matcher.matches()) {
				throw new IllegalArgumentException(String.format("Linha %s esta invalida", line));
			}

			Rota rota = StringToRota(matcher.group(1), matcher.group(2), matcher.group(3), mapa);
			rotas.add(rota);
		}
		return rotas;
	}

	/**
	 * Construir re retorna o {@link Rota}
	 * 
	 * @param origem nome ponto (cidade) origem
	 * @param destino nome ponto (cidade) destino
	 * @param distancia
	 * @param mapa nome do mapa
	 * @return {@link Rota}
	 */
	private Rota StringToRota(String origem, String destino, String distancia, String mapa) {
		return new Rota(new Cidade(origem, mapa), new Cidade(destino, mapa), new Long(distancia), mapa);
	}
}
