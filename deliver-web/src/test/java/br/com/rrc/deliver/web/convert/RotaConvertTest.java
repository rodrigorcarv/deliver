package br.com.rrc.deliver.web.convert;

import java.util.Set;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.rrc.deliver.beans.Rota;

@ContextConfiguration(locations={"classpath:spring-context-test.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class RotaConvertTest {
	
	@Autowired
	RotaConvert rotaConvert; 
	
	String nomeMapa = "Mapa";
	String coodenadas = "A B 10 \n "
			+ "B D 15 \n"
			+ "A C 20 \n"
			+ "C D 30 \n"
			+ "B E 50 \n"
			+ "D E 30 ";

	
	@Test
	public void testRotaConvert() {
		
		Set<Rota> stringToRota = rotaConvert.stringToRota(coodenadas, nomeMapa);
		
		System.out.println(stringToRota);
		Assert.assertNotNull(stringToRota);
	}
}
