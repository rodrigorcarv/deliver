package br.com.rrc.deliver.repository;

import org.springframework.data.neo4j.repository.GraphRepository;

import br.com.rrc.deliver.beans.Cidade;

public interface CidadeRepository extends GraphRepository<Cidade> {
	
	/**
	 * Busca a cidade por nome da cidade e nome do mapa
	 *
	 * @param nome da cidade
	 * @param mapa nome do mapa
	 * @return {@link}
	 */
	Cidade findByNomeAndMapa(String nome, String mapa);
}
