package br.com.rrc.deliver.repository;

import java.util.Map;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.stereotype.Repository;

import br.com.rrc.deliver.beans.Rota;

@Repository
public interface RotaRepository extends GraphRepository<Rota> {
	
	/**
	 * Busca obter a melhor de acordo com os dados informados.
	 *
	 * @param origem nome ponto (cidade) origem
	 * @param destino nome ponto (cidade) destino
	 * @param mapa nome do mapa
	 * @return the iterable
	 */
	@Query("MATCH  (from:Cidade { nome:{0} }), (to:Cidade { nome: {1}}), "
			+ "path = (from)-[:ROTA*]->(to)  "
			+ "WHERE from.mapa={2} AND to.mapa={2}"
			+ " 	RETURN path AS shortestPath, "
			+ " 	reduce(distancia = 0, r in relationships(path) | distancia+r.distancia) AS totalDistance"
			+ " ORDER BY totalDistance ASC "
			+ "	LIMIT 1")
	Iterable<Map<String, Object>> obterMelhorRota(String origem, String destino, String mapa);
	 
}

