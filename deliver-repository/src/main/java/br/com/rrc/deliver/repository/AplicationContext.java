package br.com.rrc.deliver.repository;

import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.data.neo4j.config.Neo4jConfiguration;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableNeo4jRepositories("br.com.rrc.deliver.repository")
@EnableTransactionManagement
@ComponentScan("br.com.rrc.deliver")
public class AplicationContext extends Neo4jConfiguration {

	public static final int NEO4J_PORT = 7474;

	@Override
	public SessionFactory getSessionFactory() {
		return new SessionFactory("br.com.rrc.deliver.beans");
	}


	@Override
	@Bean
	@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
	public Session getSession() throws Exception {
		return super.getSession();
	}
}
