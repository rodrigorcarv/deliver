package br.com.rrc.deliver.repository;

import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;

import br.com.rrc.deliver.beans.Cidade;

public class CidadeRepositoryTest extends AbstractBaseTest {
	
	@Autowired
	CidadeRepository cidadeRepository;
	
	@Autowired
	Session session;

	@Test
	public void testSaveCidade() {
		Cidade cidade = new Cidade("A", "Test");
		Assert.assertTrue(cidadeRepository.save(cidade).getId() != null);
	}
	
	@Test
	public void testBuscaCidadePorNomeEMapa() {
		
		Cidade cidade = new Cidade("A", "Test");
		cidadeRepository.save(cidade);
		Assert.assertNotNull(cidadeRepository.findByNomeAndMapa("A", "Test"));
	}
	
	@After
    public void tearDown() {
       session.purgeDatabase();
    }
}
