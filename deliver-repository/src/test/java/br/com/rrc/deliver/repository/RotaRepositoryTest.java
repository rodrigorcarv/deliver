package br.com.rrc.deliver.repository;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.http.client.ClientProtocolException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

import br.com.rrc.deliver.beans.Cidade;
import br.com.rrc.deliver.beans.Rota;

public class RotaRepositoryTest extends AbstractBaseTest {

	@Autowired
	RotaRepository rotaRepository;

	@Autowired
	Session session;

	@After
    public void tearDown() {
       session.purgeDatabase();
    }
	
	@Test
	public void testSaveMapa() {
		
		String mapa = "Mapa";

		Cidade cidadeA = new Cidade("A", mapa);
		Cidade cidadeB = new Cidade("B", mapa);
		Cidade cidadeC = new Cidade("C", mapa);
		Cidade cidadeD = new Cidade("D", mapa);
		Cidade cidadeE = new Cidade("E", mapa);

		Set<Rota> rotas = new HashSet<Rota>();

		rotas.add(criarRota(cidadeA, cidadeB, 10L, mapa));
		rotas.add(criarRota(cidadeB, cidadeD, 15L, mapa));
		rotas.add(criarRota(cidadeA, cidadeC, 20L, mapa));
		rotas.add(criarRota(cidadeC, cidadeD, 30L, mapa));
		rotas.add(criarRota(cidadeB, cidadeE, 50L, mapa));
		rotas.add(criarRota(cidadeD, cidadeE, 30L, mapa));
		
		rotaRepository.save(rotas);
		Assert.assertNotNull(rotas.iterator().next().getId());
	}

	@Test
	public void testObterCaminhoMaisCurto() throws ClientProtocolException, IOException {

		String origem = "A";
		String destino = "D";
		String mapa = "Mapa";
		Long totalDistanciaEsperada = 25L;
		int qtdeNodesPecorridos = 3;

		Iterable<Map<String, Object>> caminhoMaisCurto = rotaRepository.obterMelhorRota(origem, destino, mapa);
		Assert.assertNotNull(caminhoMaisCurto);

		for (Map<String, Object> map : caminhoMaisCurto) {

			JsonElement json = new Gson().toJsonTree(map);

			JsonArray asJsonArray = json.getAsJsonObject().get("shortestPath").getAsJsonObject().get("nodes").getAsJsonArray();
			System.out.println(asJsonArray);
			Assert.assertEquals(asJsonArray.size(), qtdeNodesPecorridos);
			
			Long totalDistancia  = json.getAsJsonObject().get("totalDistance").getAsLong();
			System.out.println(String.format("Total da Distancia: %s", totalDistancia));
			Assert.assertEquals(totalDistancia, totalDistanciaEsperada);
		}
	}

	private Rota criarRota(Cidade cidadeOrigem, Cidade cidadeDestino, Long distancia, String nomeMapa){
		return new Rota(cidadeOrigem, cidadeDestino, distancia, nomeMapa);
	}
}