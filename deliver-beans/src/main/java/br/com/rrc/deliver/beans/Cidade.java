package br.com.rrc.deliver.beans;

import java.io.Serializable;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

@NodeEntity
public class Cidade implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 486257848962205826L;
	
	@GraphId
	private Long id;
	private String nome;
	private String mapa;

	public Cidade(String nome,String mapa) {
		this.nome = nome;
		this.mapa =  mapa;
	}
	
	public Cidade() {}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getMapa() {
		return mapa;
	}

	public void setMapa(String mapa) {
		this.mapa = mapa;
	}

	@Override
	public String toString() {
		return String.format("Cidade [id=%s, nome=%s, mapa=%s]", id, nome, mapa);
	}
}
