package br.com.rrc.deliver.dto;

import java.math.BigDecimal;
import java.util.List;

import br.com.rrc.deliver.beans.Cidade;

public class DadosEntrega {
	
	private List<Cidade> cidades;
	private Long distancia;
	private BigDecimal custo;
	
	public List<Cidade> getCidades() {
		return cidades;
	}
	public void setCidades(List<Cidade> cidades) {
		this.cidades = cidades;
	}
	public Long getDistancia() {
		return distancia;
	}
	public void setDistancia(Long distancia) {
		this.distancia = distancia;
	}
	public BigDecimal getCusto() {
		return custo;
	}
	public void setCusto(BigDecimal custo) {
		this.custo = custo;
	}
	
	
}
