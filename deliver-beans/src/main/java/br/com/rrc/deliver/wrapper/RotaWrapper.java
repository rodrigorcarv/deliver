package br.com.rrc.deliver.wrapper;

import java.io.Serializable;
import java.util.HashSet;

import br.com.rrc.deliver.beans.Rota;

public class RotaWrapper extends HashSet<Rota> implements Serializable { 
	
	private static final long serialVersionUID = 1L;
	
	public RotaWrapper() {
		super();
	}
}
