																																																																																																																																																																																																																																																																		package br.com.rrc.deliver.dto;

import java.math.BigDecimal;

public class DadosEnvio {
	
	private String nomeMapa;
	private String origem;
	private String destino;
	private Long autonomia;
	private String valorCombustivel;	
	

	public String getNomeMapa() {
		return nomeMapa;
	}
	public void setNomeMapa(String nomeMapa) {
		this.nomeMapa = nomeMapa;
	}
	public String getOrigem() {
		return origem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public Long getAutonomia() {
		return autonomia;
	}
	public void setAutonomia(Long autonomia) {
		this.autonomia = autonomia;
	}
	public String getValorCombustivel() {
		return valorCombustivel;
	}
	public void setValorCombustivel(String valorCombustivel) {
		this.valorCombustivel = valorCombustivel;
	}
}
