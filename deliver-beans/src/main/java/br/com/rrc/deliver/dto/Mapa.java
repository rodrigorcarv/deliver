package br.com.rrc.deliver.dto;

import java.util.Set;

import br.com.rrc.deliver.beans.Rota;

public class Mapa {

	private String nome;
	private Set<Rota> rota;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Set<Rota> getRota() {
		return rota;
	}

	public void setRota(Set<Rota> rota) {
		this.rota = rota;
	}

	@Override
	public String toString() {
		return String.format("Mapa [nome=%s, rota=%s]", nome, rota);
	}
}
