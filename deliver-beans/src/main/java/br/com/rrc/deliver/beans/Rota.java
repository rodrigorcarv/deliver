package br.com.rrc.deliver.beans;

import java.io.Serializable;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Relationship;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type="ROTA")
//@XmlRootElement (name = "rota")
//@XmlAccessorType(XmlAccessType.NONE)
public class Rota implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2841635518870876322L;
	
	@GraphId
    private Long id;
	private String nomeMapa;
	
	public Rota(Cidade cidadeOrigem, Cidade cidadeDestino, Long distancia, String nomeMapa) {
		super();
		this.cidadeOrigem = cidadeOrigem;
		this.cidadeDestino = cidadeDestino;
		this.distancia = distancia;
	}
	
	public Rota(Cidade cidadeOrigem, Cidade cidadeDestino, Long distancia) {
		super();
		this.cidadeOrigem = cidadeOrigem;
		this.cidadeDestino = cidadeDestino;
		this.distancia = distancia;
	}
	
	public Rota() {}
	
	@Relationship(type = "Cidade", direction = Relationship.INCOMING)
	@StartNode
	private Cidade cidadeOrigem;
	
	@Relationship(type = "Cidade", direction = Relationship.OUTGOING)
	@EndNode
	private Cidade cidadeDestino;
	
	private Long distancia;
	
	public Cidade getCidadeOrigem() {
		return cidadeOrigem;
	}
	public void setCidadeOrigem(Cidade cidadeOrigem) {
		cidadeOrigem.setMapa(nomeMapa);
		this.cidadeOrigem = cidadeOrigem;
	}
	public Cidade getCidadeDestino() {
		return cidadeDestino;
	}
	public void setCidadeDestino(Cidade cidadeDestino) {
		cidadeDestino.setMapa(nomeMapa);
		this.cidadeDestino = cidadeDestino;
	}
	public Long getDistancia() {
		return distancia;
	}
	public void setDistancia(Long distancia) {
		this.distancia = distancia;
	}
	public Long getId() {
		return id;
	}

	public String getNomeMapa() {
		return nomeMapa;
	}
	public void setNomeMapa(String nomeMapa) {
		this.nomeMapa = nomeMapa;
	}
	@Override
	public String toString() {
		return String.format("Rota [id=%s, cidadeOrigem=%s, cidadeDestino=%s, distancia=%s]", id, cidadeOrigem,
				cidadeDestino, distancia);
	}
	
	
}
