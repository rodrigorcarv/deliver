package br.com.rrc.deliver.services;

import br.com.rrc.deliver.beans.Cidade;

public interface CidadeService {

	/**
	 * Metodo que busca a cidade por nome da cidade e nome do mapa.
	 * Se nao existir a {@link Cidade} e criada.
	 * 
	 * @param nome nome da ponto ou ciade
	 * @param mapa nome do mapa
	 * 
	 * @return {@link Cidade}
	 */
	Cidade findCidadeByNameAndMapIfNotExistsCreateCidade(String nome, String mapa);

}
