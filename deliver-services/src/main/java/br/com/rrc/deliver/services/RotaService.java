package br.com.rrc.deliver.services;

import java.math.BigDecimal;
import java.util.Set;

import br.com.rrc.deliver.beans.Rota;
import br.com.rrc.deliver.dto.DadosEntrega;
import br.com.rrc.deliver.dto.DadosEnvio;

public interface RotaService {

	/**
	 * Save uma {@link Set} de {@link Rotas} de um deteminado mapa
	 *
	 * @param rotas {@link Set} de {@link Rotas} 
	 * @param mapa nome do mapa
	 */
	void save(Set<Rota> rotas, String mapa);

	/**
	 * Shortest route deliver.
	 * 
	 * Metodo responsavel por identifcar o melhor caminho para o mapa informado
	 * Exemplo dado:
	 * mapa: Deliver
	 * Rota: 
	 *  	A B 10
	 *  	B D 15
	 *  	A C 20
	 *  	C D 30
	 *  	B E 50
	 *  	D E 30
	 *  
	 *  o metodo vai retorna melhor rota A B D distancia 25 e custo 6.25
	 *
	 * @param dadosEnvio {@link DadosEnvio}
	 * @return {@link DadosEntrega}
	 */
	DadosEntrega shortestRouteDeliver(DadosEnvio dadosEnvio);
	
	/**
	 * Obter custos 
	 * 
	 * Calcula a obtencao de custos utlizando a formula:
	 * 
	 *   (distancia * valorCombustivel) / autonomia
	 * 
	 *
	 * @param autonomia autonomia (km/l)
	 * @param valorCombustivel valor do combustivel
	 * @param distancia distancia a ser pecorrida
	 * @return valor a ser gasto.
	 */
	BigDecimal getCostDelivery(BigDecimal autonomia, BigDecimal valorCombustivel, BigDecimal distancia);		
}
