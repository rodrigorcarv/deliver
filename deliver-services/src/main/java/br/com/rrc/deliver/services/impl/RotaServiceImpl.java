package br.com.rrc.deliver.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.neo4j.graphdb.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

import br.com.rrc.deliver.beans.Cidade;
import br.com.rrc.deliver.beans.Rota;
import br.com.rrc.deliver.dto.DadosEntrega;
import br.com.rrc.deliver.dto.DadosEnvio;
import br.com.rrc.deliver.repository.CidadeRepository;
import br.com.rrc.deliver.repository.RotaRepository;
import br.com.rrc.deliver.services.CidadeService;
import br.com.rrc.deliver.services.RotaService;

/**
 * Class RotaServiceImpl responsavel por save e busca as Rotas
 */
@Service
public class RotaServiceImpl implements RotaService{

	private static final String REGEX_ID_NODE = "node/(\\d.*$)";
	
	@Autowired
	private RotaRepository rotaRepository;
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	@Autowired
	private CidadeService cidadeService;
	

	/* (non-Javadoc)
	 * @see br.com.rrc.deliver.services.RotaService#save(java.util.Set, java.lang.String)
	 */
	@Override
	public void save(Set<Rota> rotas, String mapa) {

		for (Rota rota : rotas) {
			
			Cidade cidadeOrigem = cidadeService.findCidadeByNameAndMapIfNotExistsCreateCidade(rota.getCidadeOrigem().getNome(), mapa);
			Cidade cidadeDestino = cidadeService.findCidadeByNameAndMapIfNotExistsCreateCidade(rota.getCidadeDestino().getNome(), mapa);
			
			rota.setCidadeDestino(cidadeDestino);
			rota.setCidadeOrigem(cidadeOrigem);
		}
		
		rotaRepository.save(rotas);
	}

	/* (non-Javadoc)
	 * @see br.com.rrc.deliver.services.RotaService#shortestRouteDeliver(br.com.rrc.deliver.dto.DadosEnvio)
	 */
	@Override
	public DadosEntrega shortestRouteDeliver(DadosEnvio dadosEnvio) {

		validaDadosEnvio(dadosEnvio);
		
		Iterable<Map<String, Object>> rotas = rotaRepository.obterMelhorRota(dadosEnvio.getOrigem(), dadosEnvio.getDestino(), dadosEnvio.getNomeMapa());

		if (rotas == null) {
			throw new NotFoundException(String.format("O mapa %s n\u00E3o possui a rota desejada", dadosEnvio.getNomeMapa()));
		}

		DadosEntrega obterDadosEntrega = getDataDeliver(rotas.iterator().next());
		
		if (obterDadosEntrega == null) {
			throw new RuntimeException(String.format("N\u00E3o foi encontrada nenhuma rota de entrega para o mapa %s", dadosEnvio.getNomeMapa()));
		}
		
		BigDecimal autonomia = new BigDecimal(dadosEnvio.getAutonomia());
		BigDecimal distancia = new BigDecimal(obterDadosEntrega.getDistancia());
		BigDecimal valorCombustivel = stringToBigDecimal(dadosEnvio.getValorCombustivel());
		
		BigDecimal costDelivery = getCostDelivery(autonomia, valorCombustivel, distancia);
		obterDadosEntrega.setCusto(costDelivery);
		
		return obterDadosEntrega;
	}

	/* (non-Javadoc)
	 * @see br.com.rrc.deliver.services.RotaService#getCostDelivery(java.math.BigDecimal, java.math.BigDecimal, java.math.BigDecimal)
	 */
	@Override
	public BigDecimal getCostDelivery(BigDecimal autonomia, BigDecimal valorCombustivel, BigDecimal distancia) {
		
		if (distancia == null || BigDecimal.ZERO.compareTo(distancia) >= 0) {
			throw new IllegalArgumentException("Dist\u00E2ncia n\u00E3o pode ser nula ou zero");
		}
		
		if (autonomia == null || BigDecimal.ZERO.compareTo(autonomia) >= 0) {
			throw new IllegalArgumentException("Autonomia n\u00E3o pode se nula ou zero");
		}
		
		if (valorCombustivel == null || BigDecimal.ZERO.compareTo(valorCombustivel) >= 0) {
			throw new IllegalArgumentException("Valor do Combust\u00EDvel n\u00E3o pode se nula zero");
		}
		
		return (distancia.multiply(valorCombustivel)).divide(autonomia);
	}

	/**
	 * Valida {@link DadosEnvio}
	 * 
	 * 	campo e validacoes
	 * 
	 * 		origem nao pode ser null ou vazio
	 * 		destino nao pode ser null ou vazio
	 * 		nomeMapa nao pode ser null ou vazio
	 * 		valorCombustivel nao pode ser null
	 * 		autonomia nao pode ser null ou menor que zero
	 * 
	 * @param dadosEnvio {@link DadosEnvio}
	 */
	private void validaDadosEnvio(DadosEnvio dadosEnvio) {

		if (dadosEnvio == null) {
			throw new IllegalArgumentException("Favor informa corretamente os dados de envio");
		}
		
		if (StringUtils.isBlank(dadosEnvio.getOrigem())) {
			throw new IllegalArgumentException("Favor informa corretamente o ponto (Cidade) de origim");
		}
		
		if (StringUtils.isBlank(dadosEnvio.getDestino())) {
			throw new IllegalArgumentException("Favor informa corretamente o ponto (Cidade) de destino");
		}
		
		if (StringUtils.isBlank(dadosEnvio.getNomeMapa())) {
			throw new IllegalArgumentException("Favor informa corretamente o nome do mapa");
		}
		
		if (StringUtils.isBlank(dadosEnvio.getValorCombustivel())) {
			throw new IllegalArgumentException("Favor informa corretamente o valor do combustivel");
		}

		if (dadosEnvio.getAutonomia() == null || dadosEnvio.getAutonomia() < 0) {
			throw new IllegalArgumentException("Favor informa corretamente a autonomia");
		}
	}
	
	/**
	 * Obtem as dados de entrega retornando o melhor caminho a ser percorrido.
	 *
	 * @param rota rota
	 * @return {@link DadosEntrega}
	 */
	private DadosEntrega getDataDeliver(Map<String, Object> rota) {
		
		DadosEntrega dadosEntrega = new DadosEntrega();
		
		JsonElement shortestPath = new Gson().toJsonTree(rota);
		List<String> nodes = getNodes(shortestPath);
		dadosEntrega.setCidades(getRouteDeliver(nodes));
		dadosEntrega.setDistancia(getDistance(shortestPath));
		
		return dadosEntrega;
	}
	
	/**
	 * Obtem a distancia do melhor caminho a ser pecorrido.
	 *
	 * @param shortestPath {@link JsonElement}
	 * @return Total da distancia pecorrida.
	 */
	private Long getDistance(JsonElement shortestPath) {
		return shortestPath.getAsJsonObject().get("totalDistance").getAsLong();
	}

	/**
	 * Obtem os nodes que representam o menor caminho percorrido.
	 *
	 * @param shortestPath {@link JsonElement}
	 * @return {@link List} de {@link String} contendo os nodes que representam o menor caminho percorrido
	 */
	private List<String> getNodes(JsonElement shortestPath) {
		
		JsonArray asJsonArray = shortestPath.getAsJsonObject().get("shortestPath").getAsJsonObject().get("nodes").getAsJsonArray();
		List<String> nodes = new Gson().fromJson(asJsonArray, new TypeToken<List<String>>() {}.getType());
		
		return nodes;
	}
	
	/**
	 * Obtem a lista de cidades a partir de um lista de id que representam a rota de entrega.
	 *
	 * @param rotaEntrega the rota entrega
	 * @return {@link List} de {@link Cidade}
	 */
	private List<Cidade> getRouteDeliver(List<String> rotaEntrega) {

		List<Cidade> cidades = new ArrayList<>();
		
		for (String string : rotaEntrega) {

			Pattern pattern = Pattern.compile(REGEX_ID_NODE);
			Matcher matcher = pattern.matcher(string);

			if (matcher.find()) {
				
				String nodeId = matcher.group(1);
				
				if (StringUtils.isBlank(nodeId)) {
					throw new RuntimeException("Erro ao buscar melhor rota de entrega");
				}
				
				Cidade cidade = cidadeRepository.findOne(new Long(nodeId)); 
				cidades.add(cidade);
			}
		}
		return cidades;
	}
	
	private BigDecimal stringToBigDecimal(String valorMonetario) {
		
		String valor = valorMonetario.replace(".", "").replace(",", ".");
		return new BigDecimal(valor);
	}
}
