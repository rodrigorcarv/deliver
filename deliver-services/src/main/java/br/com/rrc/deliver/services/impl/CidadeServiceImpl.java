package br.com.rrc.deliver.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.rrc.deliver.beans.Cidade;
import br.com.rrc.deliver.repository.CidadeRepository;
import br.com.rrc.deliver.services.CidadeService;

@Service
public class CidadeServiceImpl implements CidadeService{
	
	@Autowired
	private CidadeRepository cidadeRepository;
	
	/* (non-Javadoc)
	 * @see br.com.rrc.deliver.services.CidadeService#findCidadeByNameAndMapIfNotExistsCreateCidade(java.lang.String, java.lang.String)
	 */
	public Cidade findCidadeByNameAndMapIfNotExistsCreateCidade(String nome, String mapa){
		
		Cidade cidade = cidadeRepository.findByNomeAndMapa(nome, mapa);
		
		if (cidade == null) {
			return cidadeRepository.save(new Cidade(nome, mapa));
		}
		
		return cidade;
	}
	
}