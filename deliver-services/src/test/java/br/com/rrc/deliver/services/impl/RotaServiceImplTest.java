package br.com.rrc.deliver.services.impl;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.rrc.deliver.services.RotaService;

@ContextConfiguration("/spring-context-test.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class RotaServiceImplTest {
	
	@Autowired
	RotaService rotaService;
	
	@Test
	public void testGetCostDeliverySucesso() {
		
		BigDecimal autonomia = new BigDecimal("10");
		BigDecimal valorCombustivel = new BigDecimal("2.5");
		BigDecimal distancia = new BigDecimal("25");
		BigDecimal costDelivery = rotaService.getCostDelivery(autonomia, valorCombustivel, distancia);
		
		Assert.assertEquals(costDelivery, new BigDecimal(6.25));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetCostDeliverAutonomiaNull() {
		
		BigDecimal valorCombustivel = new BigDecimal("2.5");
		BigDecimal distancia = new BigDecimal("25");
		rotaService.getCostDelivery(null, valorCombustivel, distancia);
		
		Assert.fail("Deve lanca a exception de IllegalArgumentException");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetCostDeliverAutonomiaZero() {
		
		BigDecimal valorCombustivel = new BigDecimal("2.5");
		BigDecimal distancia = new BigDecimal("25");
		rotaService.getCostDelivery(BigDecimal.ZERO, valorCombustivel, distancia);
		
		Assert.fail("Deve lanca a exception de IllegalArgumentException");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetCostDeliverValorCombustivelNull() {
		
		BigDecimal autonomia = new BigDecimal("10");
		BigDecimal distancia = new BigDecimal("25");
		rotaService.getCostDelivery(autonomia, null, distancia);
		
		Assert.fail("Deve lanca a exception de IllegalArgumentException");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetCostDeliverValorCombustivelZero() {
		
		BigDecimal autonomia = new BigDecimal("10");
		BigDecimal distancia = new BigDecimal("25");
		rotaService.getCostDelivery(autonomia, BigDecimal.ZERO, distancia);
		
		Assert.fail("Deve lanca a exception de IllegalArgumentException");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetCostDeliverValorDistanciaNull() {
		
		BigDecimal autonomia = new BigDecimal("10");
		BigDecimal valorCombustivel = new BigDecimal("2.5");
		rotaService.getCostDelivery(autonomia, valorCombustivel, null);
		
		Assert.fail("Deve lanca a exception de IllegalArgumentException");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testGetCostDeliverValorDistanciaZero() {
		
		BigDecimal autonomia = new BigDecimal("10");
		BigDecimal valorCombustivel = new BigDecimal("2.5");
		rotaService.getCostDelivery(autonomia, valorCombustivel, BigDecimal.ZERO);
		
		Assert.fail("Deve lanca a exception de IllegalArgumentException");
	}
	

}
